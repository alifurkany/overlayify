# overlayify

A program to quickly make an image compatible with https://github.com/alifurkany/place-overlay

# Installation

```bash
git clone https://gitlab.com/alifurkany/overlayify
cd overlayify
npm install
```

# Usage
    
```bash
node . <image1 x y> [image2 x y] [image3 x y] ... # composite images and make a canvas
```
